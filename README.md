## INTRODUCTION

The Simple ActiveCampaign module is allowing to display simple newsletter form
through a block system.

### Features
- A block with a form that allows user to add as a contact.
- Allow created contact to be added to a selected contact list.
- Allow user to select additional custom list.

### Post-Installation
- Go to Config > Services > ActiveCampaign and enter API URL and API key.
- Go to the Block management place and place the ActiveCampaign block into a
  selected region.

### Similar projects
https://drupal.org/project/activecampaign - More features, but uses old API and doesn't support adding users to the lists.

### Maintainers

Current maintainers for Drupal 10:

- Elaman Imashov (elaman) - https://www.drupal.org/u/elaman
