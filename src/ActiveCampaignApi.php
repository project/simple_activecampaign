<?php

namespace Drupal\simple_activecampaign;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\Client;

/**
 * ActiveCampaign API service class.
 */
final class ActiveCampaignApi implements ActiveCampaignApiInterface {

  /**
   * ActiveCampaign API version.
   */
  const API_VERSION = 3;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * ActiveCampaign API object.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $apiClient;

  /**
   * Constructs an ActiveCampaignApi object.
   */
  public function __construct(
    LoggerChannelInterface $logger,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->logger = $logger;

    $config = $configFactory->get('simple_activecampaign.api_settings');
    $this->apiClient = new Client([
      'base_uri' => $config->get('api_url'),
      'headers' => [
        'Api-Token' => $config->get('api_key'),
        'accept' => 'application/json',
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getContactLists() {
    try {
      $response = $this->apiClient->get('api/' . self::API_VERSION . '/lists');
      if ($response->getStatusCode() === 200) {
        $data = Json::decode($response->getBody()->getContents());

        // Backwards compatibility.
        return array_map(function ($list) {
          return (object) $list;
        }, $data['lists']);
      }
      else {
        $this->logger->error('Unexpected response from ActiveCampaign API when fetching contact lists. Response: @code @reason. Response body: @body', [
          '@code' => $response->getStatusCode(),
          '@reason' => $response->getReasonPhrase(),
          '@body' => $response->getBody()->getContents(),
        ]);
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Unexpected exception when fetching contact lists from ActiveCampaign API. Exception message: @message', [
        '@message' => $e->getMessage(),
      ]);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateOrCreateContact($email, $firstName = '', $lastName = '', $phone = '') {
    try {
      $response = $this->apiClient->post('api/' . self::API_VERSION . '/contact/sync', [
        'json' => [
          'contact' => compact('email', 'firstName', 'lastName', 'phone'),
        ],
      ]);
      if (in_array($response->getStatusCode(), [200, 201])) {
        $data = Json::decode($response->getBody()->getContents());
        return $data['contact']['id'] ?? FALSE;
      }
      else {
        $this->logger->error('Unexpected response from ActiveCampaign API when creating or updating contact. Response: @code @reason. Response body: @body', [
          '@code' => $response->getStatusCode(),
          '@reason' => $response->getReasonPhrase(),
          '@body' => $response->getBody()->getContents(),
        ]);
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Unexpected exception when creating or updating contact in ActiveCampaign API. Exception message: @message', [
        '@message' => $e->getMessage(),
      ]);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function addContactToContactList($contact_id, $contact_list) {
    try {
      $response = $this->apiClient->post('api/' . self::API_VERSION . '/contactLists', [
        'json' => [
          'contactList' => [
            'list' => $contact_list,
            'contact' => $contact_id,
            'status' => 1,
          ],
        ],
      ]);
      if (in_array($response->getStatusCode(), [200, 201])) {
        return TRUE;
      }
      else {
        $this->logger->error('Unexpected response from ActiveCampaign API when adding contact to the contact list. Response: @code @reason. Response body: @body', [
          '@code' => $response->getStatusCode(),
          '@reason' => $response->getReasonPhrase(),
          '@body' => $response->getBody()->getContents(),
        ]);
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Unexpected exception when adding contact to the contact list in ActiveCampaign API. Exception message: @message', [
        '@message' => $e->getMessage(),
      ]);
    }

    return FALSE;
  }

}
