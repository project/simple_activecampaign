<?php

namespace Drupal\simple_activecampaign\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\simple_activecampaign\ActiveCampaignApiInterface;
use Drupal\simple_activecampaign\Form\SubscribeForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a ActiveCampaign Subscription block.
 *
 * @Block(
 *   id = "simple_activecampaign_subscribe_block",
 *   admin_label = @Translation("Simple ActiveCampaign"),
 * )
 */
class SubscribeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The FormBuilder object.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * ActiveCampaign API service.
   *
   * @var \Drupal\simple_activecampaign\ActiveCampaignApiInterface
   */
  private $activeCampaignApi;

  /**
   * Constructs a new SubscribeBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The user storage.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The FormBuilder object.
   * @param \Drupal\simple_activecampaign\ActiveCampaignApiInterface $activecampaign_api
   *   ActiveCampaign API service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, ActiveCampaignApiInterface $activecampaign_api) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->activeCampaignApi = $activecampaign_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('simple_activecampaign.api'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'simple_activecampaign' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration['simple_activecampaign'];

    // Fetch contact lists from ActiveCampaign.
    $activecampaign_contact_lists = $this->activeCampaignApi->getContactLists();
    if (!is_array($activecampaign_contact_lists)) {
      $this->messenger()
        ->addWarning($this->t('Unable to fetch contact lists from ActiveCampaign. Please contact website support.'));
      return [];
    }

    // Merge configuration with fetched contact lists.
    $contact_lists = [];
    $contact_lists_config = $config['contact_lists'] ?? [];
    foreach ($activecampaign_contact_lists as $contact_list) {
      $contact_lists[$contact_list->id] = [
        'name' => $contact_list->name,
        'label' => $contact_lists_config[$contact_list->id]['label'] ?? $contact_list->name,
        'weight' => $contact_lists_config[$contact_list->id]['weight'] ?? 0,
        'show' => $contact_lists_config[$contact_list->id]['show'] ?? 0,
      ];
    }

    $form['simple_activecampaign']['contact_list'] = [
      '#type' => 'details',
      '#title' => $this->t('Contact lists'),
      '#open' => TRUE,
    ];
    $form['simple_activecampaign']['contact_list']['default_contact_list'] = [
      '#type' => 'select',
      '#title' => $this->t('Default contact list'),
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $config['default_contact_list'] ?? '',
      '#options' => array_map(function ($contact_list) {
        return $contact_list['name'];
      }, $contact_lists),
      '#description' => $this->t('All the subscribers will be added to this list by default. It will be excluded from extra contact lists.'),
    ];

    $form['simple_activecampaign']['contact_list']['contact_lists'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Description'),
        $this->t('Show'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('No ActiveCampaign lists were found. Please check ActiveCampaign admin panel!'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];

    // Sort contact lists by weight.
    uasort($contact_lists, function ($a, $b) {
      return $a['weight'] - $b['weight'];
    });
    foreach ($contact_lists as $contact_list_id => $contact_list) {
      $form['simple_activecampaign']['contact_list']['contact_lists'][$contact_list_id] = [
        '#attributes' => ['class' => 'draggable'],
        '#weight' => $contact_list['weight'],
        'name' => [
          '#markup' => $contact_list['name'],
        ],
        'label' => [
          '#type' => 'textfield',
          '#default_value' => $contact_list['label'],
          '#required' => TRUE,
          '#states' => [
            'disabled' => [
              ':input[name="settings[simple_activecampaign][contact_list][default_contact_list]"]' => ['value' => $contact_list_id],
            ],
          ],
        ],
        'show' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Show @title', ['@title' => $contact_list['name']]),
          '#default_value' => $contact_list['show'],
          '#title_display' => 'invisible',
          // If the list is selected as default. It will be hidden.
          '#states' => [
            'invisible' => [
              ':input[name="settings[simple_activecampaign][contact_list][default_contact_list]"]' => ['value' => $contact_list_id],
            ],
          ],
        ],
        'weight' => [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => $contact_list['name']]),
          '#title_display' => 'invisible',
          '#default_value' => $contact_list['weight'],
          '#attributes' => ['class' => ['table-sort-weight']],
        ],
      ];
    }

    $form['simple_activecampaign']['form'] = [
      '#type' => 'details',
      '#title' => $this->t('Form settings'),
      '#open' => FALSE,
    ];
    $form['simple_activecampaign']['form']['email_field_label'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config['email_field_label'] ?? $this->t('Email'),
      '#title' => $this->t('Email field label'),
    ];
    $form['simple_activecampaign']['form']['contact_list_field_label'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config['contact_list_field_label'] ?? $this->t('Select a list'),
      '#title' => $this->t('Contact list field label'),
    ];
    $agreement_checkbox_label = $config['agreement_checkbox_label'] ?? [];
    $form['simple_activecampaign']['form']['agreement_checkbox_label'] = [
      '#type' => 'text_format',
      '#required' => TRUE,
      '#rows' => 1,
      '#format' => $agreement_checkbox_label['format'] ?? '',
      '#default_value' => $agreement_checkbox_label['value'] ?? $this->t("I've read and agree to your privacy policy."),
      '#title' => $this->t('Agreement checkbox label'),
    ];
    $form['simple_activecampaign']['form']['submit_button_label'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config['submit_button_label'] ?? $this->t('Subscribe'),
      '#title' => $this->t('Subscribe button label'),
    ];
    $form['simple_activecampaign']['form']['successful_message'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config['successful_message'] ?? $this->t('Thank you for subscribing to our newsletter.'),
      '#title' => $this->t('Successful subscription message'),
    ];
    $form['simple_activecampaign']['form']['failed_message'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config['failed_message'] ?? $this->t('Unable to finish subscription. Please contact website support.'),
      '#title' => $this->t('Failed subscription message'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValue('simple_activecampaign');

    $this->setConfiguration([
      'simple_activecampaign' => [
        'default_contact_list' => $values['contact_list']['default_contact_list'],
        'contact_lists' => $values['contact_list']['contact_lists'],
        'email_field_label' => $values['form']['email_field_label'],
        'contact_list_field_label' => $values['form']['contact_list_field_label'],
        'agreement_checkbox_label' => $values['form']['agreement_checkbox_label'],
        'submit_button_label' => $values['form']['submit_button_label'],
        'successful_message' => $values['form']['successful_message'],
        'failed_message' => $values['form']['failed_message'],
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build['form'] = $this->formBuilder->getForm(
      SubscribeForm::class,
      $this->configuration['simple_activecampaign']
    );

    return $build;
  }

}
