<?php

namespace Drupal\simple_activecampaign\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_activecampaign\ActiveCampaignApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an ActiveCampaign Subscription form.
 */
final class SubscribeForm extends FormBase {

  /**
   * ActiveCampaign API service.
   *
   * @var \Drupal\simple_activecampaign\ActiveCampaignApiInterface
   */
  protected ActiveCampaignApiInterface $activeCampaignApi;

  /**
   * Form constructor.
   *
   * @param \Drupal\simple_activecampaign\ActiveCampaignApiInterface $active_campaign_api
   *   The ActiveCampaign API service.
   */
  public function __construct(ActiveCampaignApiInterface $active_campaign_api) {
    $this->activeCampaignApi = $active_campaign_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_activecampaign.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'simple_activecampaign_subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config = []): array {
    $form_state->set('config', $config);

    $form['#prefix'] = '<div id="simple-activecampaign-subscribe-wrapper">';
    $form['#suffix'] = '</div>';

    $form['email'] = [
      '#type' => 'email',
      '#required' => TRUE,
      '#title' => $config['email_field_label'] ?? $this->t('Email'),
    ];

    $contact_lists = $config['contact_lists'] ?? [];
    $form['contact_list'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('- None -'),
      '#title' => $config['contact_list_field_label'] ?? $this->t('Select list'),
      '#options' => [],
    ];
    uasort($contact_lists, function ($a, $b) {
      return $a['weight'] - $b['weight'];
    });
    foreach ($contact_lists as $contact_list_id => $contact_list) {
      // Either hidden or is a default contact list.
      if ($contact_list['show'] && $contact_list_id != $config['default_contact_list']) {
        $form['contact_list']['#options'][$contact_list_id] = $contact_list['label'];
      }
    }
    // Hide if empty.
    $form['contact_list']['#access'] = !empty($form['contact_list']['#options']);

    $agreement_checkbox_label = $config['agreement_checkbox_label'] ?? [];
    $form['agreement'] = [
      '#type' => 'checkbox',
      '#required' => TRUE,
      '#title' => $agreement_checkbox_label ? check_markup(
        $agreement_checkbox_label['value'],
        $agreement_checkbox_label['format']
      ) : $this->t("I've read and agree to your privacy policy"),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $config['submit_button_label'] ?? $this->t('Subscribe'),
        '#ajax' => [
          'callback' => '::ajaxSubmitCallback',
          'wrapper' => 'simple-activecampaign-subscribe-wrapper',
          'progress' => ['type' => 'fullscreen'],
        ],
        '#validate' => [[$this, 'validateSubscription']],
      ],
    ];

    return $form;
  }

  /**
   * AJAX callback function.
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Custom validation callback to check if subscription is created.
   */
  public function validateSubscription(array &$form, FormStateInterface $form_state) {
    // We require to provide an email and check agreement checkbox.
    if ($form_state->isValueEmpty('email') || $form_state->isValueEmpty('agreement')) {
      return;
    }

    // We want to make sure there are no errors before performing API request.
    if ($form_state->getErrors()) {
      return;
    }

    // Init the API then attempt to update or create the contact.
    $config = $form_state->get('config');
    if ($activecampaign_contact_id = $this->activeCampaignApi->updateOrCreateContact($form_state->getValue('email'))) {
      // Attempt to add user to the default list.
      if ($default_contact_list = $config['default_contact_list']) {
        $is_in_default_contact_list = $this->activeCampaignApi->addContactToContactList($activecampaign_contact_id, $default_contact_list);
      }

      // Attempt to add user to the selected list.
      if (!$form_state->isValueEmpty('contact_list')) {
        $is_in_selected_contact_list = $this->activeCampaignApi->addContactToContactList($activecampaign_contact_id, $form_state->getValue('contact_list'));
      }
    }

    // Check if all API call were successful.
    if (!$activecampaign_contact_id || empty($is_in_default_contact_list) || empty($is_in_selected_contact_list)) {
      $this->messenger()->addError(
        $config['failed_message']
        ?? $this->t('Unable to finish subscription. Please contact website support.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus(
      $this->config('simple_activecampaign.settings')->get('successful_message')
        ?? $this->t('Thank you for subscribing to our newsletter.')
    );

    // Reset the form.
    $form['email']['#value'] = '';
    $form['agreement']['#checked'] = FALSE;
    $form['contact_list']['#value'] = '';
  }

}
