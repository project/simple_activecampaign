<?php

namespace Drupal\simple_activecampaign\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Simple ActiveCampaign API settings for this site.
 */
final class ApiSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'simple_activecampaign_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['simple_activecampaign.api_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('simple_activecampaign.api_settings');

    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Access'),
      '#description' => $this->t('You can get API credentials by logging into ActiveCampaign dashboard, then navigating to Settings > Developer.'),
    ];
    $form['api']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#default_value' => $config->get('api_url'),
    ];
    $form['api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#default_value' => $config->get('api_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('simple_activecampaign.api_settings')
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
