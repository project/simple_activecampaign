<?php

namespace Drupal\simple_activecampaign;

/**
 * ActiveCampaign API Service interface class.
 */
interface ActiveCampaignApiInterface {

  /**
   * Initialize API.
   *
   * @return array|bool
   *   An array of contact list objects or FALSE.
   */
  public function getContactLists();

}
